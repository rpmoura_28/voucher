<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'clients',
], function() {
    Route::get('', 'ClientController@all');
    Route::get('/filter', 'ClientController@filter');
    Route::post('', 'ClientController@store');
});

Route::group([
    'prefix' => 'offers',
], function() {
    Route::post('', 'OfferController@store');
    Route::get('{id}', 'OfferController@show');
});

Route::group([
    'prefix' => 'vouchers',
], function() {
    Route::post('', 'VoucherController@store');
    Route::get('/check', 'VoucherController@checkVoucher');
    Route::put('/use', 'VoucherController@useVoucher');
});
