<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(ClientSeeder::class);
        $offer = factory(App\Models\Offer::class)->create()->toArray();

        $voucher = factory(App\Models\Voucher::class)->make()->toArray();

        factory(App\Models\Client::class, 50)->create()->each(function ($client) use ($offer, $voucher) {
            $client->vouchers()->create(array_merge([
                "offer_id" => $offer['id'],
            ], $voucher));
        });
    }
}
