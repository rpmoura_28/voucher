<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->id();
            $table->string('voucher_code', 36);
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('offer_id');
            $table->date('expires_at');
            $table->timestamp('used_at')->nullable();
            $table->timestamps();

            // Index
            $table->index('voucher');
            // FK
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('offer_id')->references('id')->on('offers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}
