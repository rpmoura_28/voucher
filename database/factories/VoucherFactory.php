<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Client;
use App\Models\Voucher;
use Faker\Generator as Faker;

$factory->define(Voucher::class, function (Faker $faker) {
    return [
        "voucher_code" => $faker->uuid,
        "expires_at" => $faker->dateTimeBetween("now", "+7 days")->format("Y-m-d")
    ];
});
