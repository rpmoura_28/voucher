<?php


namespace App\Services;


use App\Repositories\Contracts\OfferRepositoryInterface;

class OfferService
{
    private OfferRepositoryInterface $offerRepository;

    /**
     * OfferService constructor.
     * @param OfferRepositoryInterface $offerRepository
     */
    public function __construct(OfferRepositoryInterface $offerRepository)
    {
        $this->offerRepository = $offerRepository;
    }

    public function create(array $params)
    {
        return $this->offerRepository->create($params);
    }


}
