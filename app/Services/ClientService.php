<?php


namespace App\Services;


use App\Repositories\Contracts\ClientRepositoryInterface;

class ClientService
{

    private ClientRepositoryInterface $clientRepository;

    /**
     * ClientService constructor.
     * @param ClientRepositoryInterface $clientRepository
     */
    public function __construct(ClientRepositoryInterface $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function create(array $params)
    {
        return $this->clientRepository->create($params);
    }

    public function all()
    {
        return $this->clientRepository->all();
    }

    public function filter(array $filters)
    {
        return $this->clientRepository->filter($filters);
    }
}
