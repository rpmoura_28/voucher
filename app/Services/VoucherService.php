<?php


namespace App\Services;


use App\Repositories\Contracts\VoucherRepositoryInterface;
use Illuminate\Support\Carbon;

class VoucherService
{
    private VoucherRepositoryInterface $voucherRepository;

    /**
     * VoucherService constructor.
     * @param VoucherRepositoryInterface $voucherRepository
     */
    public function __construct(VoucherRepositoryInterface $voucherRepository)
    {
        $this->voucherRepository = $voucherRepository;
    }

    public function create(array $params)
    {
        return $this->voucherRepository->create($params);
    }

    public function checkVoucher(array $params)
    {
        return $this->voucherRepository->checkVoucher($params);
    }

    public function useVoucher(array $params)
    {
       $voucher = $this->voucherRepository->checkVoucher($params);

       if (!$voucher->used_at) {
           $used_at = Carbon::now()->format("Y-m-d H:i:s");
           return $this->voucherRepository->useVoucher($voucher->id, $used_at);
       }

       return $voucher;
    }
}
