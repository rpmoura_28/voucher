<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Contracts\ClientRepositoryInterface',
            'App\Repositories\ClientRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\OfferRepositoryInterface',
            'App\Repositories\OfferRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\VoucherRepositoryInterface',
            'App\Repositories\VoucherRepository'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
