<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VoucherResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "voucher_code" => $this->voucher_code,
            "client" => ClientResource::make($this->whenLoaded('client')),
            "offer" => OfferResource::make($this->whenLoaded('offer')),
            "expires_at" => $this->expires_at,
            "used_at" => $this->used_at
        ];
    }
}
