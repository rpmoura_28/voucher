<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClientFilterRequest;
use App\Http\Requests\ClientRequest;
use App\Services\ClientService;
use Illuminate\Http\Request;

class ClientController extends Controller
{

    private ClientService $clientService;

    /**
     * ClientController constructor.
     * @param ClientService $clientService
     */
    public function __construct(ClientService $clientService)
    {
        $this->clientService = $clientService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        $data = $this->clientService->all();
        return response()->json($data, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ClientRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ClientRequest $request)
    {
        $params = $request->validated();
        $data = $this->clientService->create($params);
        return response()->json($data, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param ClientFilterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filter(ClientFilterRequest $request)
    {
        $params = $request->validated();

        $data = $this->clientService->filter($params);

        return response()->json($data, 200);
    }
}
