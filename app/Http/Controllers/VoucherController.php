<?php

namespace App\Http\Controllers;


use App\Http\Requests\VoucherRequest;
use App\Http\Requests\VoucherCheckRequest;
use App\Http\Requests\VoucherUseRequest;
use App\Services\VoucherService;

class VoucherController extends Controller
{
    private VoucherService $voucherService;

    /**
     * VoucherController constructor.
     * @param VoucherService $voucherService
     */
    public function __construct(VoucherService $voucherService)
    {
        $this->voucherService = $voucherService;
    }

    public function store(VoucherRequest $request)
    {
        $params = $request->validated();
        $data = $this->voucherService->create($params);

        return response()->json($data, 201);
    }

    public function checkVoucher(VoucherCheckRequest $request)
    {
        $params = $request->validated();
        $data = $this->voucherService->checkVoucher($params);

        return response()->json($data, 200);
    }

    public function useVoucher(VoucherCheckRequest $request)
    {
        $params = $request->validated();
        $data = $this->voucherService->useVoucher($params);

        return response()->json($data, 200);
    }
}
