<?php

namespace App\Http\Controllers;

use App\Http\Requests\OfferRequest;
use App\Services\OfferService;
use Illuminate\Http\Request;

class OfferController extends Controller
{

    private OfferService $offerService;

    /**
     * OfferController constructor.
     * @param OfferService $offerService
     */
    public function __construct(OfferService $offerService)
    {
        $this->offerService = $offerService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param OfferRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(OfferRequest $request)
    {
        $params = $request->validated();
        $data = $this->offerService->create($params);
        return response()->json($data, 201);
    }

}
