<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ("POST" === $this->getMethod()) {
            return [
                "name" => "required|string|max:300",
                "email" => "required|unique:clients,email",
            ];
        }

        return [
          'email' => 'sometimes|email',
        ];
    }
}
