<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $table = 'offers';

    protected $fillable = ['name', 'percentage'];

    protected $dates = ['expired_at'];

    public function vouchers()
    {
        return $this->hasMany('App\Models\Voucher','offer_id', 'id');
    }
}
