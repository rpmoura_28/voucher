<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = "clients";

    protected $fillable = ["name", "email"];

    public function vouchers()
    {
        return $this->hasMany('App\Models\Voucher', 'client_id', 'id');
    }
}
