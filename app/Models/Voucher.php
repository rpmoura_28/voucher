<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $table = 'vouchers';

    protected $fillable = ['voucher_code', 'client_id', 'offer_id', 'expires_at', 'used_at'];

    protected $dates = ['expires_at', 'used_at'];

    public function client()
    {
        return $this->belongsTo('App\Models\Client', 'client_id', 'id');
    }

    public function offer()
    {
        return $this->belongsTo('App\Models\Offer', 'offer_id', 'id');
    }
}
