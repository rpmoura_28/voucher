<?php


namespace App\Repositories\Contracts;


interface OfferRepositoryInterface
{

    public function create(array $params);
}
