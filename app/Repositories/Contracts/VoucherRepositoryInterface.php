<?php


namespace App\Repositories\Contracts;


interface VoucherRepositoryInterface
{
    public function create(array $params);

    public function checkVoucher(array $params);

    public function useVoucher(int $id, string $used_at);
}
