<?php


namespace App\Repositories\Contracts;


interface ClientRepositoryInterface
{

    public function all();

    public function create(array $params);

    public function filter(array $filters);
}
