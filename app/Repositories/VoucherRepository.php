<?php


namespace App\Repositories;


use App\Http\Resources\VoucherResource;
use App\Models\Voucher;
use Illuminate\Support\Carbon;

class VoucherRepository implements Contracts\VoucherRepositoryInterface
{

    public function create(array $params)
    {
        $voucher = Voucher::create($params);
        $voucher->load('client', 'offer');

        return VoucherResource::make($voucher);
    }

    public function checkVoucher(array $params)
    {
        $data = Voucher::where('voucher_code', '=', $params['voucher_code'])
            ->whereNull('used_at')
            ->whereDate('expires_at', ">=", Carbon::today()->format("Y-m-d"))
            ->whereHas('client', function ($query) use ($params) {
                $query->where('email', $params['email']);
            })
            ->with('offer:id,name,percentage')
            ->firstOrFail();

        return VoucherResource::make($data);
    }

    public function useVoucher(int $id, string $used_at)
    {
        $voucher = Voucher::find($id);
        $voucher->used_at = $used_at;
        $voucher->save();

        $voucher->load('offer');

        return VoucherResource::make($voucher);
    }
}
