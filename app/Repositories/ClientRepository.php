<?php

namespace App\Repositories;

use App\Http\Resources\ClientResource;
use App\Models\Client;
use Illuminate\Support\Carbon;

class ClientRepository implements Contracts\ClientRepositoryInterface
{

    /**
     * @param array $params
     * @return ClientResource
     */
    public function create(array $params)
    {
        return ClientResource::make(Client::create($params));
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function all()
    {
        return ClientResource::collection(Client::all());
    }


    /**
     * @param array $filters
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function filter(array $filters)
    {
        $query = Client::query();

        if (!empty($filters['email'])) {
            $query->where('email', $filters['email']);
        }

        // used ->get() instead of first(), because in future improvements
        // could use other filters, returning more than one result
        $data = $query->with(['vouchers' => function ($query)  {
            $date = Carbon::today();
            $query->whereNull('used_at')->whereDate('expires_at', '>=', $date)->with('offer');
        }])->get();

//        $data = Client::when(!empty($filters['email']), function($query) use ($filters) {
//            $query->where('email', $filters['email']);
//        })->with('vouchers.offer')->get();

        return ClientResource::collection($data);
    }
}
