<?php


namespace App\Repositories;


use App\Http\Resources\OfferResource;
use App\Models\Offer;

class OfferRepository implements Contracts\OfferRepositoryInterface
{

    public function create(array $params)
    {
        return OfferResource::make(Offer::create($params));
    }
}
