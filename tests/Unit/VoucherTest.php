<?php

namespace Tests\Unit;

use App\Models\Client;
use App\Models\Offer;
use App\Models\Voucher;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class VoucherTest extends TestCase
{

    use WithFaker;

    public function testRequiredFieldsForCreate()
    {
        $this->json("POST", "api/vouchers", ["Accept" => "application/json"])
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "voucher_code" => ["The voucher code field is required."],
                    "client_id" => ["The client id field is required."],
                    "offer_id" => ["The offer id field is required."],
                    "expires_at" => ["The expires at field is required."]
                ]
            ]);
    }

    public function testVoucherSuccessfullyCreate()
    {
        $client = factory(Client::class)->create();
        $offer = factory(Offer::class)->create();

        $data = [
            "client_id" => $client->id,
            "offer_id" => $offer->id,
            "expires_at" => $this->faker
                ->dateTimeBetween("now", "+7 days")
                ->format("Y-m-d"),
            "voucher_code" => $this->faker->text(36)
        ];

        $this->json("POST", "api/vouchers", $data, ["Accept" => "application/json"])
            ->assertStatus(201)
            ->assertJsonStructure([
                "id",
                "client",
                "offer",
                "voucher_code",
                "expires_at",
                "used_at"
            ]);
    }

    public function testVoucherUnavailable()
    {
        $offer = factory(Offer::class)->create()->toArray();
        $client = factory(Client::class)->create();

        $client->vouchers()->create([
            "offer_id" => $offer['id'],
            "voucher_code" => $this->faker->ean13,
            "expires_at" => $this->faker->dateTimeBetween("-1 month", "-1 day")
        ]);

        $data = [
            "email" => $client->email,
            "voucher_code" => $client->vouchers[0]->voucher_code
        ];

        $this->json("GET", "api/vouchers/check", $data, ["Accept" => "application/json"])
            ->assertStatus(404)
            ->assertJson([
                "error" => "The requested resource could not be found."
            ]);
    }

    public function testVoucherAvailable()
    {
        $offer = factory(Offer::class)->create()->toArray();
        $voucher = factory(Voucher::class)->make()->toArray();

        $client = factory(Client::class)->create();
        $client->vouchers()->create(
            array_merge(
                [
                    "offer_id" => $offer['id'],
                ],
                $voucher
            )
        );

        $data = [
            "email" => $client->email,
            "voucher_code" => $client->vouchers[0]->voucher_code
        ];

        $this->json("GET", "api/vouchers/check", $data)
            ->assertStatus(200)
            ->assertJsonStructure([
                "id",
                "voucher_code",
                "offer" => [
                    "id",
                    "name",
                    "percentage",
                ],
                "expires_at",
                "used_at"
            ]);
    }

    public function testVoucherUseSuccessfully()
    {
        $offer = factory(Offer::class)->create()->toArray();
        $voucher = factory(Voucher::class)->make()->toArray();

        $client = factory(Client::class)->create();
        $client->vouchers()->create(
            array_merge(
                [
                    "offer_id" => $offer['id'],
                ],
                $voucher
            )
        );

        $data = [
            "email" => $client->email,
            "voucher_code" => $client->vouchers[0]->voucher_code
        ];

        $this->json("PUT", "api/vouchers/use", $data)
            ->assertStatus(200)
            ->assertJsonStructure([
                "id",
                "voucher_code",
                "offer" => [
                    "id",
                    "name",
                    "percentage",
                ],
                "expires_at",
                "used_at"
            ]);
    }
}
