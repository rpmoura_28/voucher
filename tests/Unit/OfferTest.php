<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OfferTest extends TestCase
{
    use WithFaker;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testRequiredFieldsForCreate()
    {
        $this->json('POST', 'api/offers', ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "name" => ["The name field is required."],
                    "percentage" => ["The percentage field is required."],
                ]
            ]);
    }

    public function testOfferWithNonNumericPercentageValue()
    {
        $data = [
            "name" => $this->faker->colorName,
            "percentage" => "value",
        ];

        $this->json('POST', 'api/offers', $data, ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "percentage" => ["The percentage must be a number."],
                ]
            ]);
    }

    public function testOfferSuccessfullyCreate()
    {
        $data = [
            "name" => $this->faker->colorName,
            "percentage" => $this->faker->numberBetween(0, 100)
        ];

        $this->json('POST', 'api/offers', $data, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJsonStructure([
                "id",
                "name",
                "percentage"
            ]);
    }
}
