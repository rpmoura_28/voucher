<?php

namespace Tests\Unit;

use App\Models\Offer;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Client;

class ClientTest extends TestCase
{
    use WithFaker;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testRequiredFieldsForCreate()
    {
        $this->json('POST', 'api/clients', ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "name" => ["The name field is required."],
                    "email" => ["The email field is required."],
                ]
            ]);
    }

    public function testClientWithExistingEmail()
    {
        // create the first client
        $client = factory(Client::class)->create()->toArray();

        // send same data client to create
        $this->json('POST', 'api/clients', $client, ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "email" => ["The email has already been taken."],
                ]
            ]);
    }

    public function testClientSuccessfullyCreate()
    {
        $data = [
            "name" => "Reginaldo",
            "email" => "reginaldo.mourap@gmail.com"
        ];

        $this->json('POST', 'api/clients', $data, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJsonStructure([
                'id',
                'name',
                'email',
            ]);
    }

    public function testListAllClients()
    {
        $amount = 10;

        factory(Client::class, $amount)->create();

        $this->json('GET', 'api/clients', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonCount($amount);
    }

    public function testListClientByEmail()
    {
        $client = factory(Client::class)->create();

        $email = $client->email;

        $this->json('GET', 'api/clients/filter?email=' . $email, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonCount(1);
    }

    public function testListClientByEmailWithVoucherAvailable()
    {
        $email = $this->faker->email;

        $client = factory(Client::class)->create();

        $offer = factory(Offer::class)->create();

        $client->vouchers()->create([
            'offer_id' => $offer->id,
            'voucher_code' => $this->faker->ean13,
            'expires_at' => $this->faker->dateTimeBetween("now", "+7 days")
        ]);

        $this->json('GET', 'api/clients/filter?email=' . $email, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                '*' => [
                    'id',
                    'name',
                    'email',
                    'vouchers' => [
                        'id',
                        'voucher_code',
                        'offer' => [
                            'id',
                            'name',
                            'percentage'
                        ],
                        'expires_at',
                        'used_at'
                    ],
                ]
            ]);
    }
}
