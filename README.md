## Requirements

 - PHP 7.4

## Local Development

A Dockerfile with PHP 7.4 and Composer installed is bundled with the project to facilitate local development.

To easily bring up the local development environment, use the Docker Compose configuration:
 - Run ```docker-compose up -d --build```.
 - Run ```docker exec -it voucher-app bash```.
 - Run ```composer install```.
 - Run ```php artisan migrate:refresh --seed```.

## Docs

  - The API docs in **Postman:(https://documenter.getpostman.com/view/3227076/TVCcWUNG)**
